THIS IS WORK IN PROGRESS, EXPECT BUGS

## pi-midibox

This project aims to use a Raspberry Pi as a midi input device hooked up on to an AMP.
It is heavily inspired by MIDI-Autoconnect by Claudius Coenen (https://codeberg.org/ccoenen/MIDI-Auto-Connect).

Compared to the above project the key differences are:

- rbdrum2midi (Rockband Drum Set support)
- no ruby depencency
- uses fluidsynth as a system server
- does not depend on the "pi" user
- Ansible deployment

My personal use case and equipment is:

- Raspberry Pi 3b (a Pi 2 should also do, a Pi 1 *might* be to slow)
- Roland CUBE GX 10 Guitar AMP (any other amp, speakers or headphones should do)
- Rockband 1 Drum Set (Real MIDI instruments should also work)


## Installation

### Install a minimal raspbian

Fetch the lastest buster image (at the time of writing 2021-05-07 was the latest minimal image)

```
wget https://downloads.raspberrypi.org/raspios_lite_armhf/images/raspios_lite_armhf-2021-05-28/2021-05-07-raspios-buster-armhf-lite.zip
unzip 2021-05-07-raspios-buster-armhf-lite.zip
sudo dd if=2021-05-07-raspios-buster-armhf-lite.img of=/dev/[your sd card device here] bs=1M
```

Mount the fat partition once and enable ssh

```
mount /dev/[your sd card device here]1 /mnt
touch /mnt/ssh
umount /mnt
```

now put the card into you Pi, connect it via a LAN cable  and power it on

### Run the ansible scripts

The scripts assume that your raspberrypi is still named raspberrypi like the default

```
ansible-playbook -Kbi hosts playbook.yml

```

Now plug in your Rockband drumset and AMP and you should be ready.
