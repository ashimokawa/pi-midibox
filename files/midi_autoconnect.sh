#!/bin/sh

sleep 1

FLUID_OUTPUT=`aconnect -l -o | grep "FLUID" |  sed "s/client \([0-9]\+\):.*/\1/"`
FLUID_OUTPUT="$FLUID_OUTPUT:0"

MIDI_DEVICES=`aconnect -l -i | grep client | grep -v \'System\' | grep -v \'Midi\ Through\' |  sed "s/client \([0-9]\+\):.*/\1/"`

IFS='
'

for LINE in $MIDI_DEVICES
do
        MIDI_DEVICE="$LINE:0"
        aconnect $MIDI_DEVICE $FLUID_OUTPUT
done
